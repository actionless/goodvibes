# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the goodvibes package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: goodvibes\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-09-20 10:25+0700\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: data/io.gitlab.Goodvibes.appdata.xml.in:4
#: data/io.gitlab.Goodvibes.desktop.in:3 src/main.c:149
msgid "Goodvibes"
msgstr ""

#: data/io.gitlab.Goodvibes.appdata.xml.in:5
#: data/io.gitlab.Goodvibes.desktop.in:5
msgid "Play web radios"
msgstr ""

#: data/io.gitlab.Goodvibes.appdata.xml.in:8
msgid "Arnaud Rebillout"
msgstr ""

#: data/io.gitlab.Goodvibes.appdata.xml.in:12
msgid "Goodvibes is a simple internet radio player for GNU/Linux."
msgstr ""

#: data/io.gitlab.Goodvibes.appdata.xml.in:15
msgid ""
"It comes with every basic features you can expect from an audio player, such "
"as multimedia keys, notifications, system sleep inhibition, and MPRIS2 "
"support."
msgstr ""

#: data/io.gitlab.Goodvibes.desktop.in:4
msgid "Radio Player"
msgstr ""

#. TRANSLATORS: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/io.gitlab.Goodvibes.desktop.in:7
msgid "Audio;Radio;Player;"
msgstr ""

#. TRANSLATORS: Do NOT translate or transliterate this text (this is an icon file name)!
#: data/io.gitlab.Goodvibes.desktop.in:11
msgid "io.gitlab.Goodvibes"
msgstr ""

#: src/ui/resources/main-window.glade:47 src/ui/gv-main-window.c:221
msgid "No station selected"
msgstr ""

#: src/ui/resources/main-window.glade:63 src/ui/gv-main-window.c:244
msgid "Stopped"
msgstr ""

#: src/ui/resources/prefs-window.glade:40
msgid "Quit"
msgstr ""

#: src/ui/resources/prefs-window.glade:41
#: src/ui/resources/prefs-window.glade:650
msgid "Close"
msgstr ""

#: src/ui/resources/prefs-window.glade:54
msgid "Close Button"
msgstr ""

#: src/ui/resources/prefs-window.glade:67
msgid "Application"
msgstr ""

#: src/ui/resources/prefs-window.glade:89
msgid "Autoplay on Startup"
msgstr ""

#: src/ui/resources/prefs-window.glade:102
msgid "Custom Output Pipeline"
msgstr ""

#: src/ui/resources/prefs-window.glade:133
msgid "Apply"
msgstr ""

#: src/ui/resources/prefs-window.glade:156
msgid "Playback"
msgstr ""

#: src/ui/resources/prefs-window.glade:192
msgid "Prevent sleep while playing"
msgstr ""

#: src/ui/resources/prefs-window.glade:205
msgid "System"
msgstr ""

#: src/ui/resources/prefs-window.glade:251
msgid "Native D-Bus Server"
msgstr ""

#: src/ui/resources/prefs-window.glade:263
msgid "MPRIS2 D-Bus Server"
msgstr ""

#: src/ui/resources/prefs-window.glade:276
msgid "D-Bus"
msgstr ""

#: src/ui/resources/prefs-window.glade:292
msgid "General"
msgstr ""

#: src/ui/resources/prefs-window.glade:316
msgid "Autoset Window Height"
msgstr ""

#: src/ui/resources/prefs-window.glade:333
msgid "Theme Variant"
msgstr ""

#: src/ui/resources/prefs-window.glade:346
msgid "System Default"
msgstr ""

#: src/ui/resources/prefs-window.glade:347
msgid "Prefer Dark"
msgstr ""

#: src/ui/resources/prefs-window.glade:348
msgid "Prefer Light"
msgstr ""

#: src/ui/resources/prefs-window.glade:362
msgid "Window"
msgstr ""

#: src/ui/resources/prefs-window.glade:388
msgid "Send Notifications"
msgstr ""

#: src/ui/resources/prefs-window.glade:411
msgid "Notifications"
msgstr ""

#: src/ui/resources/prefs-window.glade:446
msgid "Console Output"
msgstr ""

#: src/ui/resources/prefs-window.glade:459
msgid "Console"
msgstr ""

#: src/ui/resources/prefs-window.glade:478
msgid "Display"
msgstr ""

#: src/ui/resources/prefs-window.glade:516
msgid "Multimedia Hotkeys"
msgstr ""

#: src/ui/resources/prefs-window.glade:529
msgid "Keyboard"
msgstr ""

#: src/ui/resources/prefs-window.glade:555
msgid "Middle Click"
msgstr ""

#: src/ui/resources/prefs-window.glade:568
msgid "Play/Stop"
msgstr ""

#: src/ui/resources/prefs-window.glade:569
msgid "Mute"
msgstr ""

#: src/ui/resources/prefs-window.glade:582
msgid "Scrolling"
msgstr ""

#: src/ui/resources/prefs-window.glade:595
msgid "Change Station"
msgstr ""

#: src/ui/resources/prefs-window.glade:596
msgid "Change Volume"
msgstr ""

#: src/ui/resources/prefs-window.glade:610
msgid "Mouse (Status Icon Mode)"
msgstr ""

#: src/ui/resources/prefs-window.glade:629
msgid "Controls"
msgstr ""

#: src/ui/resources/station-dialog.glade:12
#: src/ui/resources/station-properties-box.glade:33
msgid "Name"
msgstr ""

#: src/ui/resources/station-dialog.glade:34
#: src/ui/resources/station-properties-box.glade:45
msgid "URI"
msgstr ""

#: src/ui/resources/station-properties-box.glade:14
msgid "Station"
msgstr ""

#: src/ui/resources/station-properties-box.glade:58
msgid "Streams"
msgstr ""

#: src/ui/resources/station-properties-box.glade:70
msgid "User-Agent"
msgstr ""

#: src/ui/resources/station-properties-box.glade:82
msgid "Codec"
msgstr ""

#: src/ui/resources/station-properties-box.glade:94
msgid "Channels"
msgstr ""

#: src/ui/resources/station-properties-box.glade:106
msgid "Sample Rate"
msgstr ""

#: src/ui/resources/station-properties-box.glade:118
msgid "Bitrate"
msgstr ""

#: src/ui/resources/station-properties-box.glade:226
msgid "Metadata"
msgstr ""

#: src/ui/resources/station-properties-box.glade:245
msgid "Title"
msgstr ""

#: src/ui/resources/station-properties-box.glade:257
msgid "Artist"
msgstr ""

#: src/ui/resources/station-properties-box.glade:269
msgid "Album"
msgstr ""

#: src/ui/resources/station-properties-box.glade:281
msgid "Genre"
msgstr ""

#: src/ui/resources/station-properties-box.glade:293
msgid "Year"
msgstr ""

#: src/ui/resources/station-properties-box.glade:305
msgid "Comment"
msgstr ""

#: src/core/gv-engine.c:201 src/core/gv-station-list.c:1268
#, c-format
msgid "%s: %s"
msgstr ""

#: src/core/gv-engine.c:202
msgid "Failed to parse pipeline description"
msgstr ""

#: src/core/gv-player.c:820
#, c-format
msgid "'%s' is neither a known station or a valid URI"
msgstr ""

#: src/core/gv-station-list.c:1269
msgid "Failed to save station list"
msgstr ""

#: src/ui/gv-main-window.c:234 src/ui/gv-main-window.c:262
#: src/feat/gv-inhibitor.c:142 src/feat/gv-notifications.c:68
#: src/feat/gv-notifications.c:113
msgid "Playing"
msgstr ""

#: src/ui/gv-main-window.c:237
msgid "Connecting…"
msgstr ""

#: src/ui/gv-main-window.c:240
msgid "Buffering…"
msgstr ""

#: src/ui/gv-prefs-window.c:284
msgid "Feature disabled at compile-time."
msgstr ""

#: src/ui/gv-prefs-window.c:431
msgid "Action when the close button is clicked."
msgstr ""

#: src/ui/gv-prefs-window.c:437 src/ui/gv-prefs-window.c:500
msgid "Setting not available in status icon mode."
msgstr ""

#: src/ui/gv-prefs-window.c:441
msgid "Whether to start playback automatically on startup."
msgstr ""

#: src/ui/gv-prefs-window.c:447
msgid "Whether to use a custom output pipeline."
msgstr ""

#: src/ui/gv-prefs-window.c:453
msgid ""
"The GStreamer output pipeline used for playback. Refer to theonline "
"documentation for examples."
msgstr ""

#: src/ui/gv-prefs-window.c:469
msgid "Prevent the system from going to sleep while playing."
msgstr ""

#: src/ui/gv-prefs-window.c:474
msgid "Enable the native D-Bus server (needed for the command-line interface)."
msgstr ""

#: src/ui/gv-prefs-window.c:480
msgid "Enable the MPRIS2 D-Bus server."
msgstr ""

#. Display
#: src/ui/gv-prefs-window.c:486
msgid "Prefer a different variant of the theme (if available)."
msgstr ""

#: src/ui/gv-prefs-window.c:493
msgid ""
"Automatically adjust the window height when a station is added or removed."
msgstr ""

#: src/ui/gv-prefs-window.c:504
msgid "Show notification when the status changes."
msgstr ""

#: src/ui/gv-prefs-window.c:509
msgid "Display information on the standard output."
msgstr ""

#. Controls
#: src/ui/gv-prefs-window.c:515
msgid "Bind mutimedia keys (play/pause/stop/previous/next)."
msgstr ""

#: src/ui/gv-prefs-window.c:521
msgid "Action triggered by a middle click on the status icon."
msgstr ""

#: src/ui/gv-prefs-window.c:527
msgid "Action triggered by mouse-scrolling on the status icon."
msgstr ""

#: src/ui/gv-prefs-window.c:533
msgid "Setting only available in status icon mode."
msgstr ""

#: src/ui/gv-prefs-window.c:648
msgid "Preferences"
msgstr ""

#: src/ui/gv-station-context-menu.c:35 src/ui/gv-station-dialog.c:423
msgid "Add Station"
msgstr ""

#: src/ui/gv-station-context-menu.c:36
msgid "Remove Station"
msgstr ""

#: src/ui/gv-station-context-menu.c:37 src/ui/gv-station-dialog.c:423
msgid "Edit Station"
msgstr ""

#: src/ui/gv-station-dialog.c:281
msgid "Cancel"
msgstr ""

#: src/ui/gv-station-dialog.c:282
msgid "Save"
msgstr ""

#: src/ui/gv-station-properties-box.c:142
msgid "kpbs"
msgstr ""

#: src/ui/gv-station-properties-box.c:144
msgid "unknown"
msgstr ""

#. TRANSLATORS: we talk about nominal bitrate here.
#: src/ui/gv-station-properties-box.c:148
#: src/ui/gv-station-properties-box.c:158
msgid "nominal"
msgstr ""

#: src/ui/gv-station-properties-box.c:153
#: src/ui/gv-station-properties-box.c:159
msgid "min"
msgstr ""

#: src/ui/gv-station-properties-box.c:154
#: src/ui/gv-station-properties-box.c:160
msgid "max"
msgstr ""

#: src/ui/gv-station-properties-box.c:175
msgid "Mono"
msgstr ""

#: src/ui/gv-station-properties-box.c:178
msgid "Stereo"
msgstr ""

#: src/ui/gv-station-properties-box.c:198
msgid "kHz"
msgstr ""

#: src/ui/gv-status-icon.c:136
msgid "stopped"
msgstr ""

#: src/ui/gv-status-icon.c:139
msgid "connecting"
msgstr ""

#: src/ui/gv-status-icon.c:142
msgid "buffering"
msgstr ""

#: src/ui/gv-status-icon.c:145
msgid "playing"
msgstr ""

#: src/ui/gv-status-icon.c:148
msgid "unknown state"
msgstr ""

#: src/ui/gv-status-icon.c:155
msgid "muted"
msgstr ""

#: src/ui/gv-status-icon.c:159
msgid "vol."
msgstr ""

#: src/ui/gv-status-icon.c:166
msgid "No station"
msgstr ""

#: src/ui/gv-status-icon.c:173
msgid "No metadata"
msgstr ""

#: src/feat/gv-hotkeys.c:145
#, c-format
msgid ""
"%s:\n"
"%s"
msgstr ""

#: src/feat/gv-hotkeys.c:146
msgid "Failed to bind the following keys"
msgstr ""

#: src/feat/gv-inhibitor.c:121
msgid "Failed to inhibit system sleep"
msgstr ""

#: src/feat/gv-notifications.c:62
#, c-format
msgid "Playing %s"
msgstr ""

#: src/feat/gv-notifications.c:65
#, c-format
msgid "Playing <%s>"
msgstr ""

#: src/feat/gv-notifications.c:106
msgid "(Unknown title)"
msgstr ""

#: src/feat/gv-notifications.c:125
msgid "Error"
msgstr ""
